#!/usr/bin/env python

import sys

from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy

from peerfinder import Participant


app = Flask(__name__)
app.config.from_pyfile('config.py')
db = SQLAlchemy(app)


def show_participants():
    for p in Participant.query.order_by(Participant.id).all():
        print("[{}] {}, {} ({}) is {} "
              "and has UUID {} ({})".format(p.id,
                                            p.name,
                                            p.country,
                                            p.contact,
                                            "active" if p.active else "inactive",
                                            p.uuid,
                                            p.comment))

def toggle_participant(id):
    p = Participant.query.get(id)
    p.active = not p.active
    db.session.merge(p)
    db.session.commit()
    print("{}ctivated participant {}".format("A" if p.active else "Dea",
                                             p))


if __name__ == '__main__':
    if len(sys.argv) > 1:
        toggle_participant(int(sys.argv[1]))
    else:
        show_participants()

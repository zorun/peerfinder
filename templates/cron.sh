#!/bin/bash
#
# Measurement script for the dn42 peer finder, see http://peerfinder.polynome.dn42
# Dependencies: curl, sed, ping
#
# This script is designed to be run in cron every minute, like this:
#
#   * * * * * /home/foo/cron.sh
#
# If you also want to provide IPv6 measurements, run it a second time with:
#
#   * * * * * /home/foo/cron.sh ipv6

# Put your UUID here, and keep it secret!
UUID="{{ uuid|default('00000000-0000-0000-0000-000000000000') }}"
PEERFINDER="{{ peerfinder }}"
NB_PINGS=5
LOGFILE='/dev/null' # Set to /dev/null to only receive errors.
                    # Set to /dev/stdout to receive mail for each ping.
                    # Set to a file writable by the cron runner to record pings.
                    #  (Errors will be sent in cron mail)

# This avoids synchronisation (everybody fetching jobs and running
# measurements simultaneously)
RANDOM_DELAY=30

[ -e $LOGFILE ] || touch $LOGFILE

[ -z "$1" ] && FAMILY="ipv4" || FAMILY="ipv6"

[ "$FAMILY" = "ipv4" ] && PING="ping -n -q" || PING="ping6 -n -q"

SLEEP=$((RANDOM % RANDOM_DELAY))
sleep "$SLEEP"
curl -s "$PEERFINDER"/target/"$UUID"/"$FAMILY" > /tmp/"$FAMILY"
read -r id ip < /tmp/"$FAMILY"

# Avoid empty fields
[ -z "$id" -o -z "$ip" ] && exit

# Make sure "id" is an integer
printf "%d" "$id" > /dev/null 2>&1 || exit

# Parsing ping output, for Linux
if ! output="$($PING -c "$NB_PINGS" -- "$ip" | grep -A1 "packets transmitted")"
then
    sent=0
    received=0
    args="avgrtt=NaN"
    echo "Target $id ($ip) is unreachable"
else
    echo $output | sed -e 's#^\([0-9]*\) packets transmitted, \([0-9]*\) received.*#\1\t\2#' > /tmp/"$FAMILY"_tmp1
    read sent received < /tmp/"$FAMILY"_tmp1
    if [ "$received" -eq 0 ]
    then
        args="avgrtt=NaN"
        echo "Target $id ($ip) is unreachable"
    else
        echo $output | sed -e 's#.*rtt min/avg/max/mdev = \(.*\)/\(.*\)/\(.*\)/\(.*\) ms$#\1\t\2\t\3\t\4#' > /tmp/"$FAMILY"_tmp2
        read minrtt avgrtt maxrtt jitter < /tmp/"$FAMILY"_tmp2
        [ -z "$avgrtt" ] && exit
        echo "RTT to target $id ($ip) is $avgrtt" >> $LOGFILE
        args="minrtt=${minrtt}&avgrtt=${avgrtt}&maxrtt=${maxrtt}&jitter=${jitter}"
    fi
fi

# Report results back to peerfinder
curl -s -d "target=${id}&probes_sent=${sent}&probes_received=${received}&${args}" "$PEERFINDER"/result/report/"$UUID" >> $LOGFILE

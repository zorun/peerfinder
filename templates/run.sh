#!/bin/bash
#
# Measurement script for the dn42 peer finder, see http://peerfinder.polynome.dn42
# Dependencies: curl, sed, ping
#
# Run without argument for IPv4-only, and with an argument for IPv6-only, e.g.
#
#   ./script.sh &
#   ./script.sh ipv6 &
#
# The script will run in a loop forever, periodically fetching targets and
# reporting results back to the peerfinder.

# Put your UUID here, and keep it secret!
UUID="{{ uuid|default('00000000-0000-0000-0000-000000000000') }}"
PEERFINDER="{{ peerfinder }}"
NB_PINGS=5
DELAY=30
# This avoids synchronisation (everybody fetching jobs and running
# measurements simultaneously)
RANDOM_DELAY=30

[ -z "$1" ] && FAMILY="ipv4" || FAMILY="ipv6"

[ "$FAMILY" = "ipv4" ] && PING="ping -n -q" || PING="ping6 -n -q"

while :
do
    SLEEP=$((DELAY + RANDOM % RANDOM_DELAY))
    sleep "$SLEEP"
    curl -s "$PEERFINDER"/target/"$UUID"/"$FAMILY" > /tmp/"$FAMILY"
    read -r id ip < /tmp/"$FAMILY"
    # Avoid empty fields
    [ -z "$id" -o -z "$ip" ] && continue
    # Make sure "id" is an integer
    printf "%d" "$id" > /dev/null 2>&1 || continue
    # Parsing ping output, for Linux
    if ! output="$($PING -c "$NB_PINGS" -- "$ip" | grep -A1 "packets transmitted")"; then
        sent=0
        received=0
        args="avgrtt=NaN"
        echo "Target $id ($ip) is unreachable"
    else
        echo $output | sed -e 's#^\([0-9]*\) packets transmitted, \([0-9]*\) received.*#\1\t\2#' > /tmp/"$FAMILY"_tmp1
        read sent received < /tmp/"$FAMILY"_tmp1
        if [ "$received" -eq 0 ]
        then
            args="avgrtt=NaN"
            echo "Target $id ($ip) is unreachable"
        else
            echo $output | sed -e 's#.*rtt min/avg/max/mdev = \(.*\)/\(.*\)/\(.*\)/\(.*\) ms$#\1\t\2\t\3\t\4#' > /tmp/"$FAMILY"_tmp2
            read minrtt avgrtt maxrtt jitter < /tmp/"$FAMILY"_tmp2
            [ -z "$avgrtt" ] && continue
            echo "RTT to target $id ($ip) is $avgrtt"
            args="minrtt=${minrtt}&avgrtt=${avgrtt}&maxrtt=${maxrtt}&jitter=${jitter}"
        fi
    fi
    curl -s -d "target=${id}&probes_sent=${sent}&probes_received=${received}&${args}" "$PEERFINDER"/result/report/"$UUID"
done

dn42 peer finder


# Quickstart

- copy config.py.sample to config.py
- `pip install -r requirements.txt` (should work with python2 and python3)
- `./peerfinder.py runserver`


# Manage participants

Very crude at the moment.  List participants:

    ./manage_participants

Activate participant whose ID is 42:

    ./manage_participants.py 42

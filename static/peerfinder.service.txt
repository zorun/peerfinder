# A systemd service for DN42 peerfinder
#
# == install ==
# - Create this file as /etc/systemd/system/peerfinder.service
# - Create a user peerfinder (Debian: useradd --system peerfinder)
# - Create the directory /home/peerfinder
# - Install http://peerfinder.polyno.me/script.sh as /home/peerfinder/peer-finder.sh
# - Enable with systemctl: systemctl enable peerfinder.service
# - Start the service: systemctl start peerfinder.service
#
# == monitoring ==
# Script output will be stored in journald, you can view this with:
#  journalctl -u peerfinder.service
#
# Status can be checked with
#  systemctl status peerfinder.service

[Unit]
Description=DN42 Peer Finder

[Service]
ExecStart=/home/peerfinder/peer-finder.sh
User=peerfinder

[Install]
WantedBy=multi-user.target

